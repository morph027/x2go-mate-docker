#!/bin/bash

if grep $HOSTNAME /etc/ssh/ssh_host_ecdsa_key.pub >/dev/null 2>&1 ; then
  ssh-keygen -N "" -t rsa -f /etc/ssh/ssh_host_rsa_key
  ssh-keygen -N "" -t dsa -f /etc/ssh/ssh_host_dsa_key
  ssh-keygen -N "" -t ecdsa -f /etc/ssh/ssh_host_ecdsa_key
  ssh-keygen -N "" -t ed25519 -f /etc/ssh/ssh_host_ed25519_key
fi

chown -R x2go: /home/x2go

SERVICES=( inetutils-syslogd dbus ssh x2goserver )

for SERVICE in "${SERVICES[@]}"
do
  service "$SERVICE" start
done

tail -f /var/log/auth.log /var/log/syslog
